# 普通测量学大地测量程序
有如下两点坐标：
             （114°24′23.1455″，30°30′18.4323″，20.258）
             （114°24′23.1455″，30°30′18.4323″，54.662）
试：
1.计算两点的空间直角坐标；
2.将得到的空间直角坐标再计算大地坐标；
3.按此纬度，1″大约相当于多长距离？
4.直角坐标一般精确到毫米，按此精度，经纬度秒数应该保留几位小数？

以此为背景进行项目合成，并致力于进行代码的功能化与人性化。