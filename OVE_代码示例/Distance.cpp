#define A_A
#include"Head.h"
void Distance(long double *l1, long double *b1, long double *h1, long double *l2, long double *b2, long double *h2, long double E, long double *N)
{
	long double L1, L2, L3, B1, B2, B3;
	long double X1, X2, Y1, Y2, Z1, Z2;
	printf("\n  请输入第一个点坐标：");
	printf("\n  请输入L坐标，以 , 隔开：");
	scanf("%lf,%lf,%lf", &L1, &L2, &L3);
	printf("\n  请输入B坐标，以 , 隔开：");
	scanf("%lf,%lf,%lf", &B1, &B2, &B3);
	printf("\n  请输入H坐标：");
	scanf("%lf", &*h1);
	LB_PI(&*l1, &*b1, L1, L2, L3, B1, B2, B3);
	*N = GetN(*b1, E);
	X1 = (*N + *h1)*cos(*b1)*cos(*l1);
	Y1 = (*N + *h1)*cos(*b1)*sin(*l1);
	Z1 = ((*N*(1 - pow(E, 2)) + *h1)*sin(*b1));

	printf("\n  请输入第二个点坐标：");
	printf("\n  请输入L坐标，以 , 隔开：");
	scanf("%lf,%lf,%lf", &L1, &L2, &L3);
	printf("\n  请输入B坐标，以 , 隔开：");
	scanf("%lf,%lf,%lf", &B1, &B2, &B3);
	printf("\n  请输入H坐标：");
	scanf("%lf", &*h2);
	LB_PI(&*l2, &*b2, L1, L2, L3, B1, B2, B3);
	*N = GetN(*b2, E);
	X2 = (*N + *h2)*cos(*b2)*cos(*l2);
	Y2 = (*N + *h2)*cos(*b2)*sin(*l2);
	Z2 = ((*N*(1 - pow(E, 2)) + *h2)*sin(*b2));
	
	long double DIS;
	DIS = sqrt(pow(X1 - X2, 2) + pow(Y1 - Y2, 2) + pow(Z1 - Z2, 2));
	printf("\n  两点间距离为：%lf", DIS);
}