#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define a 6378137.0
#define f 1.0/298.257222101
#define PI 3.14159265358979323846
void XYZ_BLH(long double *x, long double *y, long double *z, long double *l, long double *b, long double *h, long double E, long double *N);
void BLH_XYZ(long double *l, long double *b, long double *h, long double *x, long double *y, long double *z, long double E, long double *N);
void LB_PI(long double *l, long double *b, long double L1, long double L2, long double L3, long double B1, long double B2, long double B3);
long double GetE(void);
long double GetN(long double b, long double E);
void Distance(long double *l1, long double *b1, long double *h1, long double *l2, long double *b2, long double *h2, long double E, long double *N);