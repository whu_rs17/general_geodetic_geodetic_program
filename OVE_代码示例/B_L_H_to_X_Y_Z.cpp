#define A_A
#include"Head.h"
void BLH_XYZ(long double *l, long double *b, long double *h, long double *x, long double *y, long double *z, long double E, long double *N)
{
	long double L1, L2, L3, B1, B2, B3;
	printf("\n  请输入L坐标，以 , 隔开：");
	scanf("%lf,%lf,%lf", &L1, &L2, &L3);
	printf("\n  请输入B坐标，以 , 隔开：");
	scanf("%lf,%lf,%lf", &B1, &B2, &B3);
	printf("\n  请输入H坐标：");
	scanf("%lf", &*h);
	LB_PI(&*l, &*b, L1, L2, L3, B1, B2, B3);
	*N = GetN(*b,E);
	*x = (*N + *h)*cos(*b)*cos(*l);
	*y = (*N + *h)*cos(*b)*sin(*l);
	*z = ((*N*(1 - pow(E, 2)) + *h)*sin(*b));
	printf("\n  该点的（X，Y，Z)坐标为：（ %lf , %lf , %lf ）\n", *x, *y, *z);
}