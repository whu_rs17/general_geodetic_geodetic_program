#define A_A
#include"Head.h"
void XYZ_BLH(long double *x, long double *y, long double *z, long double *l, long double *b, long double *h, long double E, long double *N)
{
	printf("\n  请输入X坐标：");
	scanf("%lf", &*x);
	printf("\n  请输入Y坐标：");
	scanf("%lf", &*y);
	printf("\n  请输入Z坐标：");
	scanf("%lf", &*z);
	if (*x == 0)                                             //获取L坐标
	{
		if (*y == 0) { *l = 0; }
		if (*y > 0) { *l = 90; }
		if (*y < 0) { *l = 180;}
	}
	else if (*x < 0 && *y>0)
	{
		*l = 180 + (atan((*y) / (*x)) * 180 / PI);
	}
	else if (*x < 0 && *y < 0)
	{
		*l = 180 + (atan((*y) / (*x)) * 180 / PI);
	}
	else if (*x > 0 && *y < 0)
	{
		*l = 360 + (atan((*y) / (*x)) * 180 / PI);
	}
	else
	{
		*l = atan((*y) / (*x));
	}
	
	long double T = sqrt(pow(*x, 2) + pow(*y, 2));
	*b = atan(*z / T);
	*N = a / sqrt(1 - pow(E, 2)*pow(sin(*b), 2));
	bool Break;
	do
	{
		//*N = ((tan(*b)*T - *z) / (pow(E, 2)*sin(*b)));
		long double middle = 0;
		middle = atan((*z + ((*N) * (pow(E, 2)) * (sin(*b)))) / T);
		Break = (fabs(*b - middle) < 1e-12);
		*b = middle;
		*N = a / sqrt(1 - pow(E, 2)*pow(sin(*b), 2));
	} while (!Break);
	*h = (T / cos(*b)) - *N;
	*b = *b * 180 / PI;

	long L1, L2, B1, B2;
	long double L3, B3;
	L1 = (long)*l;
	L2 = (long)((*l - L1) * 60);
	L3 = ((*l - L1) * 60 - L2) * 60;
	B1 = (long)*b;
	B2 = (long)((*b - B1) * 60);
	B3 = ((*b - B1) * 60 - B2) * 60;
	printf("\n  该点的（B，L，H）坐标为：（ %ld°%ld′%lf″, %ld°%ld′%lf″, %lf ）", L1, L2, L3, B1, B2, B3, *h);
}