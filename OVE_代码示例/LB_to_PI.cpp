#define A_A
#include"Head.h"
void LB_PI(long double *l, long double *b, long double L1, long double L2, long double L3, long double B1, long double B2, long double B3)
{
	*l = (L1 + (L2 / 60) + (L3 / 3600)) / 180 * PI;
	*b = (B1 + (B2 / 60) + (B3 / 3600)) / 180 * PI;
}