#ifndef MODE_H_
#define MODE_H_
#include"BaseCacu.h"
#include"Caculation.h"
#include<iomanip>
void Explain();//解释说明
void ModeOne(); //模式一：将大地坐标系转为空间直角坐标系(已验证)
void ModeTwo();//模式二：将空间直角坐标系转为大地坐标系(已验证)
void ModeThree();//计算两点距离，待验证

#endif // !MODE_H_

