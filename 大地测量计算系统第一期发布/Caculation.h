#ifndef CACUL_H_
#define CACUL_H_
#include"BaseCacu.h"

void CaculationGeodetic(LD X,LD Y,LD Z,LD *L,LD *B,LD *H);//计算L，B，H，令L、B的转化为角度制在函数之外(已验证)
void CaculationSpace(LD L, LD B, LD H, LD *X, LD *Y, LD *Z);//计算X，Y，Z，令L、B在函数之外转化为弧度制再导入(已验证)
LD CacuB(LD X,LD Y,LD Z,LD *N);//计算B的弧度值，产生弧度制的结果 (已验证）
LD LongitudeSexCaculation(LD X1,LD Y1,LD Z1,LD X2,LD Y2,LD Z2);//输入两点直角坐标进行距离计算(已验证)

#endif // !CACUL_H_

