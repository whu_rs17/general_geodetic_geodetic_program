#include"Caculation.h"

LD CacuB(LD X, LD Y, LD Z, LD *N)//产生弧度制的结果 (已验证）
{
	LD E = ECaculation();
	LD B = atan(Z / sqrt(pow(X, 2) + pow(Y, 2)));
	LD b = 0.0;
	LD temp = 0.0;
	LD c = 0.00000000001;
	/*for (int i=0;i<1000000;i++) {
	b = atan((Z + sin(B)*E*a / sqrt(1 - E * pow(sin(B), 2))) / sqrt(pow(X, 2) + pow(Y, 2)));
	B = b;
	} */
	do {
		*N = a / sqrt(1 - E * pow(sin(B), 2));
		b = atan((Z + (*N)*E*sin(B)) / sqrt(pow(X, 2) + pow(Y, 2)));
		temp = abs(B - b);
		B = b;
	} while (temp>c);
	*N = a / sqrt(1 - E * pow(sin(B), 2));
	return B;
}

void CaculationGeodetic(LD X, LD Y, LD Z, LD *L, LD *B, LD *H) //令L、B的转化在函数之外(已验证)
{
	LD E = ECaculation();
	LD N = 0.0;
	*L = atan(Y / X);
	if (X <= 0 && Y <= 0)
		*L -= PI;
	if (X <= 0 && Y >= 0)
		*L += PI;
	*B = CacuB(X, Y, Z, &N);
	*H = sqrt(pow(X, 2) + pow(Y, 2)) / cos(*B) - N;
}

void CaculationSpace(LD L, LD B, LD H, LD *X, LD *Y, LD *Z) //令L、B的转化在函数之外再导入(已验证)
{
	LD E = ECaculation();
	LD N = a / sqrt(1 - E * pow(sin(B), 2));
	*X = (N + H)*cos(B)*cos(L);
	*Y = (N + H)*cos(B)*sin(L);
	*Z = (N*(1 - E) + H)*sin(B);
}

LD LongitudeSexCaculation(LD X1, LD Y1, LD Z1, LD X2, LD Y2, LD Z2)//输入两点直角坐标进行距离计算(已验证)
{
	return sqrt(pow(X1 - X2, 2) + pow(Y1 - Y2, 2) + pow(Z1 - Z2, 2));
}

