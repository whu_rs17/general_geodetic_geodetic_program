﻿#include"BaseCacu.h"
/*换算函数集*/
LD ECaculation(void)//计算e²(已验证)
{
	LD b = a - a * f;
	LD E = (pow(a, 2) - pow(b, 2)) / pow(a, 2);
	return E;
}

LD Conversion(LD num,int flag) //角度与弧度互相转化(已验证)
{
	LD re_num = 0.0;
	if (flag == 1) //角度转弧度
	{
		re_num = PI * num / 180;
	}
	else if (flag == 2) //弧度转角度
	{
		re_num = 180 * num / PI;
	}
	else
		cout << "Wrong Flag Value!\n\n";
	return re_num;
}

LD RAinputLimitation(LD num1, LD num2, LD num3) //将度分秒转化为弧度制(已验证)
{
	LD store1 = num2 / 60;
	LD store2 = num3 / 3600;
	LD sumstore = num1 + store1 + store2;
	return Conversion(sumstore, 1);
}

void RAoutputLimitation(LD num, LD *de, LD *mi, LD *se)//将弧度制转化为度分秒(已验证)
{
	LD store = Conversion(num, 2);
	LD store1 = 0.0, store2 = 0.0, sec = 0.0;
	*de = (int)store;
	store1 = 60 * (store - *de);
	*mi = (int)store1;
	*se = 60 * (store1 - *mi);
}