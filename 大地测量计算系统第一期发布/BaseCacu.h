﻿#ifndef BASE_H_
#define BASE_H_
#define _CRT_SECURE_NO_DEPRECATE
#include<iostream>
#include<cmath>
using namespace std;
#define a 6378137.0
#define f 1.0/298.257222101
#define PI 3.1415926535
typedef long double LD;

LD ECaculation(void);//计算e²(已验证)
LD Conversion(LD num, int flag); //角度与弧度互相转化(已验证)
LD RAinputLimitation(LD num1, LD num2, LD num3); //将度分秒转化为弧度制(已验证)
void RAoutputLimitation(LD num, LD *de, LD *mi, LD *se);//将弧度制转化为度分秒(已验证)
#endif // !BASE_H_

