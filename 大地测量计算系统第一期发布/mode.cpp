#include"MODE.h"

void Explain() {
	cout << "\t\t______******Welcome to use Geodetic System!******______\n"
		<< "Your have three choices:\n"
		<< "1)(L,B,H)--->(X,Y,Z)\n2)(X,Y,Z)--->(L,B,H)\n3)Distance Count\n4)Quit\n\n";
}

void ModeOne() //模式一：将大地坐标系转为空间直角坐标系(已验证)
{
	LD L, L1, L2, L3, B, B1, B2, B3, H, X, Y, Z;
	cout << "\nThis mode is for (L,B,H)→(X,Y,Z), please input your degree by this:\n "
		<< "(A°B′C″)→(A,B,C)\n"
		<< "Enter Your Longitude:  "; scanf_s("%Lf,%Lf,%Lf", &L1, &L2, &L3);
	cout << "Enter Your Latitude:  ";scanf_s("%Lf,%Lf,%Lf", &B1, &B2, &B3);
	cout << "Enter Your Height:  "; cin >> H;
	L = RAinputLimitation(L1, L2, L3);
	B = RAinputLimitation(B1, B2, B3);
	CaculationSpace(L, B, H, &X, &Y, &Z);
	cout << fixed;
	cout << endl << "In Space Rectangular Coordinate System, The Piont Is: " << endl
		<< "(" << X << " , " << Y << " , " << Z << ")";
	cout << endl;
}

void ModeTwo()  //模式二：将空间直角坐标系转为大地坐标系(已验证)
{
	LD L, L1, L2, L3, B, B1, B2, B3, H, X, Y, Z;
	cout << "\nThis mode is for (X,Y,Z)→(L,B,H), please input your coordinate:\n";
	cout<<"Enter Your X number:  "; cin>>X;
	cout << "Enter Your Y number:  "; cin>>Y;
	cout << "Enter Your Z number:  "; cin >> Z;
	CaculationGeodetic(X,Y,Z,&L,&B,&H);
	RAoutputLimitation(L, &L1, &L2, &L3);
	RAoutputLimitation(B, &B1, &B2, &B3);
	cout << endl << "In Geodetic Coordinate System, The Piont Is: " << endl
		<< "(" << L1<<"°"<<L2<<"′"<<L3<<"″" << " , " 
		<< B1 << "°" << B2 << "′" << B3 << "″"  << " , "
		<< H << ")";
	cout << endl;
}

void ModeThree()//计算两点距离，待验证
{
	LD L = 0.0, L1 = 0.0, L2 = 0.0, L3 = 0.0, B = 0.0, B1 = 0.0, B2 = 0.0, B3 = 0.0, H = 0.0;
	LD L_ = 0.0, L1_ = 0.0, L2_ = 0.0, L3_ = 0.0, B_ = 0.0, B1_ = 0.0, B2_ = 0.0, B3_ = 0.0, H_ = 0.0;
	LD X1 = 0.0, Y1 = 0.0, Z1 = 0.0, X2 = 0.0, Y2 = 0.0, Z2 = 0.0;
	LD len = 0.0;
	char code = 0;
	cout << "\nThis mode is for the caculation between two points, please choose your coordinate mode:\n"
		<<"A)(X,Y,Z)\nB)(B,L,H)\n"
		<< "Input Your Option:___\b\b";
	cin >> code; 
	cin.get();
	switch (code) {
	case 'A':case 'a':
		cout << "\nplease input your coordinate by (X,Y,Z):\n"
			<< "Enter Your First Point:\n"
			<< "Enter Your X number:  ";
		cin >> X1;
		cout << "Enter Your Y number:  "; 
		cin >> Y1;
		cout << "Enter Your Z number:  "; 
		cin >> Z1;
		cout << "Enter Your Second Point:\n"
			<< "Enter Your X number:  "; 
		cin >> X2;
		cout << "Enter Your Y number:  "; 
		cin >> Y2;
		cout << "Enter Your Z number:  ";
		cin >> Z2;
		len = LongitudeSexCaculation(X1, Y1, Z1, X2, Y2, Z2); 
		cout << "\nThe distance between two points is " << len << "m.\n";
		break;
	case 'B':case 'b':
		cout << "\nplease input your coordinate by (L,B,H),input them by (A°B′C″)→(A,B,C):\n"
			<< "Enter Your First Point:\n"
			<< "Enter Your Longitude:  ";
		scanf_s("%Lf,%Lf,%Lf", &L1, &L2, &L3);
		cout << "Enter Your Latitude:  "; 
		scanf_s("%Lf,%Lf,%Lf", &B1, &B2, &B3);
		cout << "Enter Your Height:  ";
		cin >> H;
		cout << "Enter Your Second Point:\n"
			<< "Enter Your Longitude:  "; 
		scanf_s("%Lf,%Lf,%Lf", &L1_, &L2_, &L3_);
		cout << "Enter Your Latitude:  "; 
		scanf_s("%Lf,%Lf,%Lf", &B1_, &B2_, &B3_);
		cout << "Enter Your Height:  "; 
		cin >> H_;
		L = RAinputLimitation(L1, L2, L3); B = RAinputLimitation(B1, B2, B3);
		L_ = RAinputLimitation(L1_, L2_, L3_); B_ = RAinputLimitation(B1_, B2_, B3_);
		CaculationSpace(L, B, H, &X1, &Y1, &Z1); 
		CaculationSpace(L_, B_, H_, &X2, &Y2, &Z2);
		len = LongitudeSexCaculation(X1, Y1, Z1, X2, Y2, Z2); 
		cout << "\nThe distance between two points is " << len << "m.\n";
		break;
	default:
		cout << endl << "Wrong code, system will quit this mode!\n\n"; 
		break;
	}

}